const express = require('express');
const bodyParser = require('body-parser');
const mockRequestedList =  require('./MockData.js')
const mockBookings =  require('./BookingMockData.js')
const app = express();

const requestedList = mockRequestedList.mockRequestedList;
const bookings = mockBookings.mockBookings;

app.use(bodyParser.json());

// CORS Headers => Required for cross-origin/ cross-server communication
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    res.setHeader(
        'Access-Control-Allow-Methods',
        'GET, POST, PATCH, DELETE, OPTIONS'
    );
    next();
});

app.get('/drivers', (req, res, next) => {
    res.status(200).json({ drivers: requestedList });
});

app.post('/estimateRide', (req, res, next) => {
    const {id, toLatitude, toLongitude, fromLatitude, fromLongitude} = req.body;

    const booking = {
        id: id,
        duration: 330,
        toLatitude: toLatitude,
        toLongitude: toLongitude,
        fromLatitude: fromLatitude,
        fromLongitude: fromLongitude
    };

    requestedList.push(booking)

    res
        .status(201)
        .json({ message: 'Created new booking.', booking: booking });
});

app.post('/bookRide', (req, res, next) => {
    const {id, toLatitude, toLongitude, fromLatitude, fromLongitude} = req.body;

    const booking = {
        id: id,
        bookingId: "yt59xt4s",
        duration: 330,
        toLatitude: toLatitude,
        toLongitude: toLongitude,
        fromLatitude: fromLatitude,
        fromLongitude: fromLongitude
    };

    /*const booking = {
        id: id,
        toLatitude: toLatitude,
        toLongitude: toLongitude,
        fromLatitude: fromLatitude,
        fromLongitude: fromLongitude
    };*/

    bookings.push(booking)

    res
        .status(201)
        .json({ message: 'Created new booking.', booking: booking });
});

app.post('/status', (req, res, next) => {
    const {id} = req.body;

    const booking = {
        id: 2,
        bookingId: "yt59xt4s",
        duration: 200,
        toLatitude: 234234234,
        toLongitude: 4324324324324,
        fromLatitude: 4324234234234,
        fromLongitude: 324234234234
    };

    /*const booking = {
        id: id,
        toLatitude: toLatitude,
        toLongitude: toLongitude,
        fromLatitude: fromLatitude,
        fromLongitude: fromLongitude
    };*/

    bookings.push(booking)

    res
        .status(201)
        .json({ message: 'Created new booking.', booking: booking });
});

app.listen(5000); // start Node + Express server on port 5000
