In the project directory, start with:

- Go to frontend map(project/frontend) in terminal and type “npm install”
- Go to backend map(project/backend) in terminal and type “npm install” 

- To start the project, do a “npm start” from both frontend and backend directory.  

- Open http://localhost:3000 to view it in the browser.
