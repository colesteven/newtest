import React, {useState, useEffect} from 'react';
import './Home.css';
import BookingGrid from "./BookingGrid/BookingGrid";
import BookingManager from "./BookingManager/BookingManager";
import EstimatedRideGrid from "./EstimatedRideGrid/EstimatedRideGrid";
import axios from "axios";


const Home = () => {

    const [estimatedRideList, setEstimatedRideList] = useState([]);
    const [bookings, setBookings] = useState([{
        id: 1,
        bookingId: "dsss33h54",
        duration: 300,
        toLatitude: 234234234,
        toLongitude: 4324324324324,
        fromLatitude: 4324234234234,
        fromLongitude: 324234234234
    }]);
    const [statusUpdate, setStatusUpdate] = useState(false);
    const [isLoading, setIsLoading] = useState(false);


    const statusHandler = async (item) => {

        const id = {
            id: item.id,
        };

        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'errrbufff'
        }

        try {

            const result = await axios.post('http://localhost:5000/status', id, {
                headers: headers
            })


         let newBookingList = bookings.filter(item => {
             console.log(item.bookingId);
             console.log(result.data.booking.bookingId);
            return  !item?.bookingId == result.data?.bookingId;

         })

            newBookingList.push(result.data.booking);
            console.log(newBookingList)
            setBookings(newBookingList);

        } catch (error) {
            alert(error.message || 'Something went wrong!');
        }

    }

    useEffect(() => {

        let timer = setInterval(() => {

            if(bookings.length >= 1) {

                bookings.map(item => {


                    statusHandler(item);
                });

            }

        }, 15000);
        return () => clearInterval(timer); // cleanup the timer

    }, [bookings]);





/*    useEffect(() => {
        const fetchDrivers = async () => {
            setIsLoading(true);
            const response = await fetch('http://localhost:5000/bookings');

            const responseData = await response.json();

            setEstimatedRideList(responseData.bookings);
            setIsLoading(false);
        };

        fetchDrivers();
    }, []);*/

/*    useEffect(() => {
        const fetchDrivers = async () => {
            setIsLoading(true);
            const response = await fetch('http://localhost:5000/bookings');

            const responseData = await response.json();

            setBookings(responseData.bookings);
            setIsLoading(false);
        };

        fetchDrivers();
    }, []);*/


    const estimateTravelHandler = async (toLatitude, toLongitude, fromLatitude, fromLongitude) => {

        const id = getRandomizer(1, 100);

        const newBooking = {
            id: id.toString(),
            toLatitude: toLatitude,
            toLongitude: toLongitude,
            fromLatitude: fromLatitude,
            fromLongitude: fromLongitude
        };

        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'errrbufff'
        }

        try {

        const result = await  axios.post('http://localhost:5000/estimateRide', newBooking, {
            headers: headers
        })

        setEstimatedRideList(prevState => ([...prevState, result.data.booking]))

        } catch (error) {
            alert(error.message || 'Something went wrong!');
        }
    }

    const bookTravel = async (item) => {
        // toLatitude, toLongitude, fromLatitude, fromLongitude

        const newBooking = {
            id: item.id,
            toLatitude: item.toLatitude,
            toLongitude: item.toLongitude,
            fromLatitude: item.fromLatitude,
            fromLongitude: item.fromLongitude
        };

        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'errrbufff'
        }

        try {

            const result = await  axios.post('http://localhost:5000/bookRide', newBooking, {
                headers: headers
            })

            addBooking(result.data.booking);

        } catch (error) {
            alert(error.message || 'Something went wrong!');
        }
    }

    const addBooking = (item) => {

        setBookings(prevState => ([...prevState, item]))

        const idToRemove= item.id;

        const updatedList = estimatedRideList.filter(function(element) {
                return element.id !== idToRemove;
            });

        setEstimatedRideList(updatedList);
    }

    const removeBooking = (id) => {
        const updatedList = bookings.filter(function(element) {
            return element.id !== id;
        });

        setBookings(updatedList);

    }

    function getRandomizer(bottom, top) {
        return Math.floor((Math.random()*100) + 1);
    }

  /*  const estimateTravelHandler = async (id, name, date) => {
        try {
            const newBooking = {
                id: id,
                name: name,
                date: date
            };
            let hasError = false;
            const response = await fetch('http://localhost:5000/booking', {
                method: 'POST',
                body: JSON.stringify(newBooking),
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            if (!response.ok) {
                hasError = true;
            }

            const responseData = await response.json();

            if (hasError) {
                throw new Error(responseData.message);
            }

            setBookings(responseData.bookings);
        } catch (error) {
            alert(error.message || 'Something went wrong!');
        }
    };*/

    return (
        <React.Fragment>
            <main>
                <div className={"homeContainer"}>
                    <div className={"bookingContainer"}>
                        <BookingManager estimateTravelHandler={estimateTravelHandler} />
                        <EstimatedRideGrid items={estimatedRideList} bookTravel={bookTravel} />
                    </div>
                    <BookingGrid isLoading={isLoading} items={bookings} removeBooking={removeBooking} />
                </div>
            </main>
        </React.Fragment>
        )
}

export default Home;
