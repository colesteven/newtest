import React, {useEffect} from 'react';
import './BookingCard.css';

const BookingCard = (props) => {

    return (
        <>
            <div className="bookingCard">
                <p className="bookingId">{props.item.bookingId}</p>
                <p className="bookingName">{props.item.name}</p>
                <p className="bookingEstimate"> {props.item.duration} </p>
                <button className={"delete"} onClick={() => {props.removeBooking(props.item.id)}}>Delete</button>
            </div>
        </>
    );
};

export default BookingCard;
